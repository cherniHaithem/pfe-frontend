export interface DCGet {
    article: string,
    userName: string,
    weekNB: number,
    ecart: number,
    done: boolean
}