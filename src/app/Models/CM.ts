import { pME } from "./pME";

export interface CM {
    article: string,
    production: number,
    userName: string,
    weekNB: number,
    pmEs: pME[]
}
