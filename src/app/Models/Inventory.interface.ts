export interface Inventory {
    article: string;
    referenceName: string;
    objectifNB: number;
    nBCOmptage: number;
    ecart: number;
    year: number;
    months: string[];
    quantity: number;
}

