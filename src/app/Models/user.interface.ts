export interface User{
    userName: string;
    email: string;
    password: string;
    role: string;
}

export interface Role{
    name :string ;
}