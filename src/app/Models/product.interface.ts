export interface Product {
    article: string;
    description: string;
    referenceName: string;
    price: number;
}