export interface Notif {
    article: string,
    weekNB: number,
    ecartPiece: number,
    ecartPrix: number,
    ecartPerCent: number,
    justif: string,
    accepted: boolean

}