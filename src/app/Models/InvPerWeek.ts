export interface InvPerWeek {
    weekNB: number;
    article: string;
    referenceName: string;
    done: boolean;
    ecart: number;
}