export interface ecart {
    article: string,
    weekNB: number,
    ecart: number,
    ecartPrice: number,
    ecartPerCent: number
}