import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ResponsableMagasinComponent } from './components/responsable-magasin/responsable-magasin.component';
import { RPLComponent } from './components/rpl/rpl.component';
import { USCMComponent } from './components/uscm/uscm.component';
import { DirecteurITComponent } from './components/directeur-it/directeur-it.component';
import { LoginComponent } from './components/login/login.component';
import { ROUTES, RouterModule } from '@angular/router';
import { ProductComponent } from './components/responsable-magasin/product/product.component';
import { InventoryComponent } from './components/responsable-magasin/inventory/inventory.component';
import { AddProductComponent } from './components/responsable-magasin/add-product/add-product.component';
import { EditProductComponent } from './components/responsable-magasin/edit-product/edit-product.component';
import { NewInvComponent } from './components/responsable-magasin/new-inv/new-inv.component';
import { InvPerYearComponent } from './components/responsable-magasin/inventory/inv-per-year/inv-per-year.component';
import { InvPerMonthComponent } from './components/responsable-magasin/inventory/inv-per-month/inv-per-month.component';
import { InvPerWeekComponent } from './components/responsable-magasin/inventory/inv-per-week/inv-per-week.component';
import { PaginationComponent } from './components/responsable-magasin/inventory/inv-per-week/pagination/pagination.component';
import { UsersComponent } from './components/responsable-magasin/users/users.component';
import { UserComponent } from './components/responsable-magasin/users/user/user.component';
import { AddUserComponent } from './components/responsable-magasin/users/add-user/add-user.component';
import { LocationComponent } from './components/responsable-magasin/location/location.component';
import { ComptageComponent } from './components/responsable-magasin/comptage/comptage.component';
import { EcartComponent } from './components/rpl/ecart/ecart.component';
import { InvComponent } from './components/rpl/inv/inv.component';
import { AllComponent } from './components/rpl/ecart/all/all.component';
import { SupCinqComponent } from './components/rpl/ecart/sup-cinq/sup-cinq.component';
import { SupCinqCentComponent } from './components/rpl/ecart/sup-cinq-cent/sup-cinq-cent.component';
import { PagiRPLComponent } from './components/rpl/inv/pagi-rpl/pagi-rpl.component';
import { UpdateLocationComponent } from './components/responsable-magasin/update-location/update-location.component';
import { NdInvComponent } from './components/responsable-magasin/inventory/nd-inv/nd-inv.component';
import { UpdateNdInvComponent } from './components/responsable-magasin/inventory/update-nd-inv/update-nd-inv.component';
import { NotifDComponent } from './components/directeur-it/notif-d/notif-d.component';
import { NotifUComponent } from './components/uscm/notif-u/notif-u.component';
@NgModule({
  declarations: [
    LoginComponent,
    ResponsableMagasinComponent,
    RPLComponent,
    USCMComponent,
    DirecteurITComponent,
    AppComponent,
    ProductComponent,
    InventoryComponent,
    UsersComponent,
    AddProductComponent,
    EditProductComponent,
    NewInvComponent,
    InvPerYearComponent,
    InvPerMonthComponent,
    InvPerWeekComponent,
    PaginationComponent,
    UserComponent,
    AddUserComponent,
    LocationComponent,
    ComptageComponent,
    EcartComponent,
    InvComponent,
    AllComponent,
    SupCinqComponent,
    SupCinqCentComponent,
    PagiRPLComponent,
    UpdateLocationComponent,
    NdInvComponent,
    UpdateNdInvComponent,
    NotifDComponent,
    NotifUComponent
  ],
  
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot([])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
