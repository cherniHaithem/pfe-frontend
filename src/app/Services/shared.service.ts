import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  userRole: string | null;
  constructor() { }
  setUserRole(role: string | null): void {
    this.userRole = role;
  }

  getUserRole(): string | null {
    return this.userRole;
  }
}
