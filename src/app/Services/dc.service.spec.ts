import { TestBed } from '@angular/core/testing';

import { DCService } from './dc.service';

describe('DCService', () => {
  let service: DCService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DCService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
