import { TestBed } from '@angular/core/testing';

import { ComptageService } from './comptage.service';

describe('ComptageService', () => {
  let service: ComptageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ComptageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
