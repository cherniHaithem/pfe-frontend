import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { DC } from '../Models/DC';
import { DCGet } from '../Models/DCGet';
import { Notif } from '../Models/Notif';

@Injectable({
  providedIn: 'root'
})
export class DCService {
  apiUrl: string = environment.apiUrl;
  constructor(private http: HttpClient) { }

  addDC(dc: DC): Observable<any>{
    return this.http.post<any>(this.apiUrl + '/api/NdInv/Add' , dc)
  }
  getNdComptage(): Observable<DCGet[]>{
    return this.http.get<DCGet[]>(this.apiUrl + '/api/NdInv/GetAll')
  }
  sendNotif(notif: Notif): Observable<Notif>{
    return this.http.post<Notif>(this.apiUrl + '/api/Notification/Add', notif)
  }
  /*getNotif(article: string): Observable<Notif[]>{
    return this.http.get<Notif[]>(this.apiUrl + '/api/Notification/notif' + article)
  }*/
  getNotif(article: string, weekNB: number): Observable<Notif>{
    return this.http.get<Notif>(`${this.apiUrl}/api/Notification/notif?article=${article}&weekNB=${weekNB}`)
  }
  getNotifD(): Observable<Notif[]>{
    return this.http.get<Notif[]>(this.apiUrl + '/api/Notification/ForDIT')
  }
  getNotifUSCM(): Observable<Notif[]>{
    return this.http.get<Notif[]>(this.apiUrl + '/api/Notification/ForUSCM')
  }
  updateNotif(article:string,weekNB:number): Observable<any>{
    return this.http.put<any>(`${this.apiUrl}/api/Notification/notif?article=${article}&weekNB=${weekNB}`, null)
  }

}
