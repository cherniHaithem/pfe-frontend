import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Role, User } from '../Models/user.interface'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  apiUrl: string = environment.apiUrl;
  
  constructor(private http: HttpClient) { }

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.apiUrl + '/api/User/GetUserRole');
  }
  getUserRole(): Observable<Role[]> {
    return this.http.get<Role[]>(this.apiUrl + '/api/PersonRole/GetAllRole' )
  }
  postUser(AddUser): Observable<User>{
    return this.http.post<User>(this.apiUrl + '/api/User/Add', AddUser);
  }
  
}
