import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CM} from '../Models/CM';

@Injectable({
  providedIn: 'root'
})
export class ComptageService {
  apiUrl: string = environment.apiUrl;
  constructor(private http: HttpClient) { }
   
  getComptage(Article: string, WeekNB: number ): Observable<CM> {
    return this.http.get<CM>(`${this.apiUrl}/api/Comptage/Comptage?Article=${Article}&weekNB=${WeekNB}`);  
  }
  getNdComptage(Article: string, WeekNB: number ): Observable<CM> {
    return this.http.get<CM>(`${this.apiUrl}/api/NdComptage/Comptage?Article=${Article}&weekNB=${WeekNB}`);  } 
}
