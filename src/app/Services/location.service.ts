import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { pME } from '../Models/pME';

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  apiUrl: string = environment.apiUrl;
  constructor(private http: HttpClient) { }

  addLocation(pme: pME): Observable<pME> {
    console.log(pme);
    return this.http.post<pME>(this.apiUrl + '/api/ProdMagEmp/Add/', pme);
  }
  updateLocation(pmEs:pME[]): Observable<pME[]> {
    return this.http.put<pME[]>(this.apiUrl + '/api/ProdMagEmp', pmEs)
  }
  
}
