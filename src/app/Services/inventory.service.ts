import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Inventory } from '../Models/Inventory.interface';
import { InvPerWeek } from '../Models/InvPerWeek';
import { ecart } from '../Models/ecart';
import { ArticleWeek } from '../Models/ArticleWeek';


@Injectable({
  providedIn: 'root'
})
export class InventoryService {
  apiUrl: string = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getInventories(): Observable<Inventory[]> {
    return this.http.get<Inventory[]>(this.apiUrl + '/api/Inventory/GetAll');
  }
  getDoneInv(): Observable<InvPerWeek[]> {
    return this.http.get<InvPerWeek[]>(this.apiUrl + '/api/InvPerWeek/GetDone/')
  }

  AddInventories(Year: number): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/api/Inventory/Add?year=${Year}`, null);
  }
  getEcart(): Observable<ecart[]> {
    return this.http.get<ecart[]>(this.apiUrl + '/api/Comptage/Ecart');
    
  }
  getNdEcart(): Observable<ecart[]> {
    return this.http.get<ecart[]>(this.apiUrl + '/api/NdComptage/Ecart');
    
  }
 
  getInvByMonth(selectedMonth: string): Observable<Inventory[]> {
    return this.http.get<Inventory[]>(`${this.apiUrl}/api/Inventory/GetByMonth?month=${selectedMonth}`);
  }

  getInvPerWeek(NBWeek: number): Observable<InvPerWeek[]> {
    return this.http.get<InvPerWeek[]>(`${this.apiUrl}/api/InvPerWeek/GetByWeek?week=${NBWeek}`);
  }
  getAllInvPerWeek(): Observable<InvPerWeek[]> {
    return this.http.get<InvPerWeek[]>(this.apiUrl + '/api/InvPerWeek/GetAll')
  }
  
  AddInvPerWeek(): Observable<InvPerWeek[]> {
    return this.http.get<InvPerWeek[]>(this.apiUrl + '/api/InvPerWeek/Add')
  }
  
  postInvLine(aw: ArticleWeek): Observable<any>{
    return this.http.post<any>(this.apiUrl + '/api/InvPerWeek/AddInv/' , aw)
  }

  deleteInv(aw: ArticleWeek): Observable<any>{
    return this.http.delete<any>(this.apiUrl + '/api/InvPerWeek/DeleteInv/' + aw )
  }
}


