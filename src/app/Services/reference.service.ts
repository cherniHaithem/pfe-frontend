import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { reference } from '../Models/reference.interface';

@Injectable({
  providedIn: 'root'
})
export class ReferenceService {
  apiUrl: string = environment.apiUrl;
  constructor(private http: HttpClient) { }
  getAllReferences(): Observable<reference[]> {
    return this.http.get<reference[]>(this.apiUrl + '/api/Reference/GetAll');
  }
  
}
