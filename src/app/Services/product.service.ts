import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../Models/product.interface';
import { environment } from 'src/environments/environment';
import { pME } from '../Models/pME';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  apiUrl: string = environment.apiUrl;
  constructor(private http: HttpClient) { }
  
  getAllProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.apiUrl + '/api/Product/GetAll');
  }

  postProduct(addProduct: Product): Observable<Product>{
    return this.http.post<Product>(this.apiUrl + '/api/Product/Add', addProduct);
  }

  getProduct(article: string): Observable<Product> {
    return this.http.get<Product>(this.apiUrl + '/api/Product/' + article)
  }

  updateProduct(Article: string , updatedProduct: Product): Observable<Product> {
    return this.http.put<Product>(this.apiUrl + '/api/Product/' + Article , updatedProduct)
  }

  deleteProduct(article: string): Observable<Product> {
    return this.http.delete<Product>(this.apiUrl + '/api/Product/Delete/' + article)
  }

  getLocation(article: string): Observable<pME[]> {
    return this.http.get<pME[]>(this.apiUrl + '/api/ProdMagEmp/' + article)
  } 
  
}
