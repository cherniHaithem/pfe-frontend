import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { SharedService } from './shared.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService,private sharedService: SharedService,private router: Router) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const requiredRole = "ResponsableMagasin"; 
      
      const userRole = this.sharedService.getUserRole();
      const hasRequiredRole = userRole === requiredRole;
      if (hasRequiredRole) {
        return true; 
      }
      if (this.authService.isLoggedIn){
      return true;
    } else {
      this.router.navigate(['*']);
      return false;
    }
  }
  
}
