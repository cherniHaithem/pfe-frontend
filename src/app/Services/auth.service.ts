import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Role, User } from '../Models/user.interface';
import { SharedService } from './shared.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private userRoleSubject: BehaviorSubject<string | null> = new BehaviorSubject<string | null>(null);
  public userRole$: Observable<string | null> = this.userRoleSubject.asObservable();

  apiUrl: string = environment.apiUrl;
  isLoggedIn = false;
  loggedInUser: User | null = null;
  constructor(private http: HttpClient, private sharedService: SharedService) {}
  
  /*getUserRole(): Observable<any>{
    return this.http.get<any>(this.apiUrl+'/api/User/Role')
  }*/
  login(userObj): Observable<any> {
    return this.http.post<any>(this.apiUrl+'/api/User/Login', userObj).pipe(
      tap(response => {
        this.isLoggedIn = true;
        this.loggedInUser = response.user; 
      })
    );
    
  }
  logout(): Observable<any>{
    this.isLoggedIn = false;
    this.loggedInUser = null;
    return this.http.post<any>(this.apiUrl + '/api/User/logout', {})
  }
  /*getUserRole(): Observable<Role[]> {
    return this.http.get<Role[]>(this.apiUrl + '/api/PersonRole/GetAllRole' )
  }*/
}
 