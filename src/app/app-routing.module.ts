import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResponsableMagasinComponent } from './components/responsable-magasin/responsable-magasin.component';
import { RPLComponent } from './components/rpl/rpl.component';
import { DirecteurITComponent } from './components/directeur-it/directeur-it.component';
import { USCMComponent } from './components/uscm/uscm.component';
import { LoginComponent } from './components/login/login.component';
import { ProductComponent } from './components/responsable-magasin/product/product.component';
import { InventoryComponent } from './components/responsable-magasin/inventory/inventory.component';
import { AddProductComponent } from './components/responsable-magasin/add-product/add-product.component';
import { EditProductComponent } from './components/responsable-magasin/edit-product/edit-product.component';
import { InvPerYearComponent } from './components/responsable-magasin/inventory/inv-per-year/inv-per-year.component';
import { InvPerMonthComponent } from './components/responsable-magasin/inventory/inv-per-month/inv-per-month.component';
import { InvPerWeekComponent } from './components/responsable-magasin/inventory/inv-per-week/inv-per-week.component';
import { NewInvComponent } from './components/responsable-magasin/new-inv/new-inv.component';
import { UsersComponent } from './components/responsable-magasin/users/users.component';
import { UserComponent } from './components/responsable-magasin/users/user/user.component';
import { AddUserComponent } from './components/responsable-magasin/users/add-user/add-user.component';
import { LocationComponent } from './components/responsable-magasin/location/location.component';
import { ComptageComponent } from './components/responsable-magasin/comptage/comptage.component';
import { EcartComponent } from './components/rpl/ecart/ecart.component';
import { InvComponent } from './components/rpl/inv/inv.component';
import { AllComponent } from './components/rpl/ecart/all/all.component';
import { SupCinqComponent } from './components/rpl/ecart/sup-cinq/sup-cinq.component';
import { SupCinqCentComponent } from './components/rpl/ecart/sup-cinq-cent/sup-cinq-cent.component';
import { UpdateLocationComponent } from './components/responsable-magasin/update-location/update-location.component';
import { NdInvComponent } from './components/responsable-magasin/inventory/nd-inv/nd-inv.component';
import { UpdateNdInvComponent } from './components/responsable-magasin/inventory/update-nd-inv/update-nd-inv.component';
import { NotifDComponent } from './components/directeur-it/notif-d/notif-d.component';
import { NotifUComponent } from './components/uscm/notif-u/notif-u.component';
import { AuthGuard } from './Services/auth.guard';

const routes: Routes = [
  {path: '', redirectTo:'Login', pathMatch: 'full'} ,
  
  {path:'Login', component: LoginComponent },
  {path:'ResponsableMagasin', component: ResponsableMagasinComponent, children: [
    {path:'Product', component: ProductComponent},
    {path:'AddProduct', component: AddProductComponent},
    {path:'inventory', component: InventoryComponent, children: [
      {path: '', redirectTo: 'InvPerYear', pathMatch: 'full'},
      {path: 'InvPerYear', component: InvPerYearComponent},
      {path: 'InvPerMonth', component: InvPerMonthComponent},
      {path: 'InvPerWeek', component: InvPerWeekComponent},
      {path: 'NdInv', component: NdInvComponent},
      {path: 'UpdateNdInv/:article/:weekNB', component: UpdateNdInvComponent}
    ]},
    {path:'UpdateLocation/:article', component: UpdateLocationComponent},
    {path: 'NewInv', component: NewInvComponent},
    {path: 'Users', component: UsersComponent, children: [
      { path: '', redirectTo: 'user', pathMatch: 'full' },
      {path:'user', component: UserComponent},
      {path:'AddUser', component: AddUserComponent}
    ]},
    {path:'Comptage/:article/:weekNB', component: ComptageComponent},
    {path:'EditProduct/:article', component: EditProductComponent},
    {path:'Location/:article', component: LocationComponent}
  ]},
  {path:'RPL', component: RPLComponent, children:[
    {path:'', redirectTo: 'Ecart', pathMatch: 'full'},
    {path:'Ecart', component: EcartComponent, children:[
      {path:'All', component: AllComponent},
      {path:'SupCinq', component: SupCinqComponent},
      {path:'SupCinqCent', component: SupCinqCentComponent}
    ]},
    {path:'Inv', component: InvComponent}
  ]},
  {path:'DirecteurIT', component: DirecteurITComponent, children: [
    {path:'NotifD', component: NotifDComponent}
  ]},
  {path:'USCM', component: USCMComponent, children:[
    {path:'NotifU', component: NotifUComponent}
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export { Routes };

