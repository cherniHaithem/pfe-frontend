import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DirecteurITComponent } from './directeur-it.component';

describe('DirecteurITComponent', () => {
  let component: DirecteurITComponent;
  let fixture: ComponentFixture<DirecteurITComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DirecteurITComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DirecteurITComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
