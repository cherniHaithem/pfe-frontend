import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotifDComponent } from './notif-d.component';

describe('NotifDComponent', () => {
  let component: NotifDComponent;
  let fixture: ComponentFixture<NotifDComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotifDComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NotifDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
