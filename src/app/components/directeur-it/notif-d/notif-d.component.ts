import { Component, OnInit } from '@angular/core';
import { Notif } from 'src/app/Models/Notif';
import { DCService } from 'src/app/Services/dc.service';

@Component({
  selector: 'app-notif-d',
  templateUrl: './notif-d.component.html',
  styleUrls: ['./notif-d.component.css']
})
export class NotifDComponent implements OnInit {

  constructor(private dcService: DCService) { }
  notifs: Notif[]=[];
  ngOnInit(): void {
    this.dcService.getNotifD().subscribe({
      next: (notifs) => {
        this.notifs = notifs
      }
    })
  }
  Accept(article: string,weekNB: number){
    this.dcService.updateNotif(article,weekNB).subscribe({
      next: (any)=>{
        alert ("notification accepted")
      },
      error(error){
        console.log(error)
      }
    })
  }
}
