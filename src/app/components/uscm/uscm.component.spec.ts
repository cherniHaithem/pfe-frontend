import { ComponentFixture, TestBed } from '@angular/core/testing';

import { USCMComponent } from './uscm.component';

describe('USCMComponent', () => {
  let component: USCMComponent;
  let fixture: ComponentFixture<USCMComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ USCMComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(USCMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
