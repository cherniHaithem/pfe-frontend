import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotifUComponent } from './notif-u.component';

describe('NotifUComponent', () => {
  let component: NotifUComponent;
  let fixture: ComponentFixture<NotifUComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotifUComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NotifUComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
