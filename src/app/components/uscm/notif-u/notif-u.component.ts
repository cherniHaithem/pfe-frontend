import { Component, OnInit } from '@angular/core';
import { Notif } from 'src/app/Models/Notif';
import { DCService } from 'src/app/Services/dc.service';

@Component({
  selector: 'app-notif-u',
  templateUrl: './notif-u.component.html',
  styleUrls: ['./notif-u.component.css']
})
export class NotifUComponent implements OnInit {

  
  
  constructor(private dcService: DCService) { }
  notifs: Notif[]=[];
  ngOnInit(): void {
    this.dcService.getNotifUSCM().subscribe({
      next: (notifs) => {
        this.notifs = notifs
      }
    })
  }
  Accept(article: string,weekNB: number){
    this.dcService.updateNotif(article,weekNB).subscribe({
      next: (any)=>{
        alert ("notification accepted")
      },
      error(error){
        console.log(error)
      }
    })
  }
}
