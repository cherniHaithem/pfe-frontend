import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../Services/auth.service';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/Services/shared.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;

  constructor(private fb:FormBuilder, public authService: AuthService,private sharedService: SharedService, private router: Router){
  
    //email: string = "";
    //password: string = "";  
    //http: HttpClient;
    
  }
  

  
  ngOnInit() {
    this.loginForm = this.fb.group ({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
    
  }

  onSubmit() {
    
    this.authService.login(this.loginForm.value)
    .subscribe({
      next:(res => {
        alert(res)
        if (res == "ResponsableMagasin"){
          this.router.navigate(['/ResponsableMagasin']);
        }else if (res == "RPL") {
          this.router.navigate(['/RPL']);
        } else if (res == "Uscm") {
          this.router.navigate(['/USCM']);
        } else if (res == "DirecteurIT") {
          this.router.navigate(['/DirecteurIT']);
        } else {
          this.router.navigate(['/']);
        }
      }),
        error:(err) => {
        alert(err?.error.message);
        }
    })
      
  }
}
