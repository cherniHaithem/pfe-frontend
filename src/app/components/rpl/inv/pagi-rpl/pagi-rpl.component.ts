import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pagi-rpl',
  templateUrl: './pagi-rpl.component.html',
  styleUrls: ['./pagi-rpl.component.css']
})
export class PagiRPLComponent implements OnInit {
  @Input() currentPage: number = 1;
  @Input() totalWeeks: number = 1;
  @Output() pageChanged: EventEmitter<number> = new EventEmitter<number>();
  constructor() { }

  ngOnInit(): void {
  }
  get totalPages(): number {
    return Math.ceil(this.totalWeeks);
  }

  getVisiblePages(): number[] {
    const startPage = Math.max(1, this.currentPage - 1);
    const endPage = Math.min(this.totalPages, this.currentPage + 1);

    const visiblePages = [];
    for (let i = startPage; i <= endPage; i++) {
      visiblePages.push(i);
    }
    return visiblePages;
  }

  onPageChanged(pageNumber: number): void {
    this.pageChanged.emit(pageNumber);
  }  
}
