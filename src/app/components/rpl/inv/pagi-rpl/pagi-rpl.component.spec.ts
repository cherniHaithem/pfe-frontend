import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PagiRPLComponent } from './pagi-rpl.component';

describe('PagiRPLComponent', () => {
  let component: PagiRPLComponent;
  let fixture: ComponentFixture<PagiRPLComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PagiRPLComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PagiRPLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
