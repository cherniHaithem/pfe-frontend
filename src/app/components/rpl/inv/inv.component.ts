import { Component, OnInit } from '@angular/core';
import { InvPerWeek } from 'src/app/Models/InvPerWeek';
import { InventoryService } from 'src/app/Services/inventory.service';

@Component({
  selector: 'app-inv',
  templateUrl: './inv.component.html',
  styleUrls: ['./inv.component.css']
})
export class InvComponent implements OnInit {
  
  NBWeek: number = 1;
  index: number = 0;
  totalWeeks: number = 48;
  currentPage: number = 1;
  constructor(private inventoryService: InventoryService) { }
  Invs: InvPerWeek[] = [];
  invs: InvPerWeek[] = [];
  total: number;
  done: number;
  perCent: number;
  Total: number;
  Done: number;
  PerCent: number;
  ngOnInit(): void {
    this.inventoryService.getAllInvPerWeek().subscribe({
      next: (Invs) => {
        this.Invs = Invs;
        this.total = this.Invs.length;
        this.done = this.Invs.filter(item => item.done == true || item.ecart != 0).length;
        this.perCent = (this.done * 100) / this.total; 
        console.log(this.total, this.done, this.perCent);
      }
    })
    this.inventoryService.getInvPerWeek(this.currentPage).subscribe ({
      next: (invs) => {
        this.invs = invs; 
        this.Total = this.invs.length;
        this.Done = this.invs.filter(item => item.done == true || item.ecart != 0).length;
        this.PerCent = (this.Done * 100) / this.Total; 
        console.log(this.Total, this.Done, this.PerCent);
      },
      error: (Response) => {
        console.log('Error: ', Error);
      }
    });
  }
  loadInventories(): void{
    this.inventoryService.getInvPerWeek(this.currentPage).subscribe ({
      next: (invs) => {
        this.invs = invs; 
        this.Total = this.invs.length;
        this.Done = this.invs.filter(item => item.done == true || item.ecart != 0).length;
        this.PerCent = (this.Done * 100) / this.Total; 
        console.log(this.Total, this.Done, this.PerCent);
      },
      error: (Response) => {
        console.log('Error: ', Error);
      }
    });
  }
  onPageChanged(pageNumber: number): void {
    this.currentPage = pageNumber;
    this.loadInventories();
  }
}
