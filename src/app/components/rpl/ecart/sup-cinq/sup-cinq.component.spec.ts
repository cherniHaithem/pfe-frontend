import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SupCinqComponent } from './sup-cinq.component';

describe('SupCinqComponent', () => {
  let component: SupCinqComponent;
  let fixture: ComponentFixture<SupCinqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SupCinqComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SupCinqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
