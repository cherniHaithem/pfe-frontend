import { Component, OnInit } from '@angular/core';
import { InvPerWeek } from 'src/app/Models/InvPerWeek';
import { ecart } from 'src/app/Models/ecart';
import { InventoryService } from 'src/app/Services/inventory.service';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.css']
})
export class AllComponent implements OnInit {
  ecars: ecart[] = [];
  Invs: InvPerWeek[] = [];
  constructor(private inventoryService: InventoryService) { }
  num: number;
  n: number;
  
  ngOnInit(): void {
    this.inventoryService.getEcart().subscribe({
      next: (ecars) => {
        this.ecars = ecars; 
        this.n = this.ecars.length;
        console.log(this.ecars.length, this.n)
        this.inventoryService.getDoneInv().subscribe({
          next: (Invs) => {
            this.Invs = Invs;
            this.num = (this.n * 100) / this.Invs.length;
            console.log(this.n, this.Invs.length);
          },
          error: (error) => {
            console.log('Error fetching inventories:', error);
          }
        })
      },
      error: (error) => {
        console.log('Error fetching inventories:', error);
      }
    });
    
   
    
  }
  
  

}
