import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SupCinqCentComponent } from './sup-cinq-cent.component';

describe('SupCinqCentComponent', () => {
  let component: SupCinqCentComponent;
  let fixture: ComponentFixture<SupCinqCentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SupCinqCentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SupCinqCentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
