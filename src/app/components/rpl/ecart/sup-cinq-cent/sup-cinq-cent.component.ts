import { Component, OnInit } from '@angular/core';
import { InvPerWeek } from 'src/app/Models/InvPerWeek';
import { ecart } from 'src/app/Models/ecart';
import { InventoryService } from 'src/app/Services/inventory.service';

@Component({
  selector: 'app-sup-cinq-cent',
  templateUrl: './sup-cinq-cent.component.html',
  styleUrls: ['./sup-cinq-cent.component.css']
})
export class SupCinqCentComponent implements OnInit {

  constructor(private inventoryService: InventoryService) { }
  ecars: ecart[] = [];
  Invs: InvPerWeek[] = [];
  num: number;
  n: number;
  ngOnInit(): void {
    this.inventoryService.getEcart().subscribe({
      next: (ecars) => {
        this.ecars = ecars.filter(item => item.ecartPrice<=-500); 
        this.n = this.ecars.length;
        console.log(this.ecars.length, this.n);
        this.inventoryService.getDoneInv().subscribe({
          next: (Invs) => {
            this.Invs = Invs;
            this.num = (this.ecars.length * 100) / this.Invs.length;
            console.log(this.n, this.Invs.length);
          },
          error: (error) => {
            console.log('Error fetching inventories:', error);
          }
        })
      },
      error: (error) => {
        console.log('Error fetching inventories:', error);
      }
    });
    
  }

}
