import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RPLComponent } from './rpl.component';

describe('RPLComponent', () => {
  let component: RPLComponent;
  let fixture: ComponentFixture<RPLComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RPLComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RPLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
