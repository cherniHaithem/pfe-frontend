import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Role, User } from 'src/app/Models/user.interface';
import { UserService } from 'src/app/Services/user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  AddUser: User = {
    userName: '',
    email: '',
    password: '',
    role: ''
  }
  selectedRole: string;

  constructor(private userService: UserService, private router: Router) { }
  roles: Role[] = [];
  ngOnInit(): void {
    this.userService.getUserRole().subscribe({
      next: (roles) => {
        this.roles=roles;
        console.log(roles);
      },
      error: (Response) => {
        console.log(Response)
      }
    })
  }
  selectRole(role: { name: string }) {
    this.AddUser.role = role.name
    this.selectedRole = role.name;
    console.log(this.AddUser.role)
  }
  addUser(){
    console.log(this.AddUser)
    this.userService.postUser(this.AddUser).subscribe({
      next: (User) => {
        console.log(User)
        this.router.navigate(['../ResponsableMagasin/Users']);
      },
      error: (error) => {
        console.error('An error occurred while saving the User:', error);
        alert("Failed to add User");
        this.router.navigate(['../ResponsableMagasin/Users']);
      }
    })
    
  }
}
