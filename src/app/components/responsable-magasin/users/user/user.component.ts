import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/Services/user.service';
import { Role, User } from 'src/app/Models/user.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  users : User[] = [];
  
  constructor(private userService: UserService, private router:Router) { }

  ngOnInit(): void {
    this.userService.getAllUsers()
    .subscribe({
      next: (users) => {
        this.users = users;
        console.log(users);
      },
      error: (Response) => {
        console.log(Response)
      }
    })
  }
  
 
}




