import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/Models/product.interface';
import { ProductService } from 'src/app/Services/product.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  productDetails: Product = {
    article: '',
    description: '',
    referenceName: '',
    price: 0
  }
  constructor(private route: ActivatedRoute, private productService: ProductService, private router: Router) {}
  private Article: string;
  ngOnInit(): void {
    this.route.paramMap.subscribe({
      next: (params) => {
        this.Article = params.get('article');
        if(this.Article) {
          this.productService.getProduct(this.Article)
          .subscribe({
            next: (response) => {
              this.productDetails = response;
            }
          })
        }
      }
    })
  }
  updateProduct() {

    this.productService.updateProduct(this.Article, this.productDetails)
    .subscribe({
      next: (response) => {
        this.router.navigate(['../ResponsableMagasin/Product']);
      }
    });
  }

  deleteProduct() {
    this.productService.deleteProduct(this.Article).subscribe({
      next:() => {
        
        alert("Product deleted");
        this.router.navigate(['../ResponsableMagasin/Product']);
      
      },
      error: (error) => {
        console.error('An error occurred while deleting the product:', error);
        
        this.router.navigate(['../ResponsableMagasin/Product']);
      }
    })
  }

}
