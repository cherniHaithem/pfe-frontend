import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Inventory } from 'src/app/Models/Inventory.interface';
import { Product } from 'src/app/Models/product.interface';
import { InventoryService } from 'src/app/Services/inventory.service';
import { ProductService } from 'src/app/Services/product.service';

@Component({
  selector: 'app-new-inv',
  templateUrl: './new-inv.component.html',
  styleUrls: ['./new-inv.component.css']
})
export class NewInvComponent implements OnInit {
  Year: number;
  products: Product[] = [];
  selectedProducts: Product[] = [];
  constructor(private productService: ProductService, private inventoryService: InventoryService, private router: Router) { }
  
  ngOnInit(): void {
    this.productService.getAllProducts()
    .subscribe({
      next: (products) => {
        this.products = products;
      },
      error: (Response) => {
        console.log(Response)
      }
    })
  }
 
  addToSelectedProducts(product: any) {
    if (product.checked) {
      this.selectedProducts.push(product);
    } else {
      const index = this.selectedProducts.findIndex((p) => p.article === product.article);
      if (index !== -1) {
        this.selectedProducts.splice(index, 1);
      }
    }
  }
  generateInventories() {
    

    
    console.log(this.selectedProducts);

    
  }
  addInventories() {
    console.log(this.selectedProducts);
  };
}
