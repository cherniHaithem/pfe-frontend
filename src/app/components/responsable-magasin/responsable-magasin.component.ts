import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Services/auth.service';

@Component({
  selector: 'app-responsable-magasin',
  templateUrl: './responsable-magasin.component.html',
  styleUrls: ['./responsable-magasin.component.css']
})
export class ResponsableMagasinComponent implements OnInit {
  name: string = "souhail"
  constructor(private authService: AuthService, private router: Router) {}
  

  ngOnInit(): void {
   let name: string = "souhail";
  }
  logout(): void {
    this.authService.logout().subscribe({
      next: (any) => {
        this.router.navigate(['/login']);
      },
      error: (error) => {
        console.error('Logout failed:', error);
        this.router.navigate(['/login']);
      }
    });
  } 
}
