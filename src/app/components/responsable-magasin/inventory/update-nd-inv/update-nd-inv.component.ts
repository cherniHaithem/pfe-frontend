import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticleWeek } from 'src/app/Models/ArticleWeek';
import { CM } from 'src/app/Models/CM';
import { Notif } from 'src/app/Models/Notif';
import { ecart } from 'src/app/Models/ecart';
import { pME } from 'src/app/Models/pME';
import { User } from 'src/app/Models/user.interface';
import { ComptageService } from 'src/app/Services/comptage.service';
import { DCService } from 'src/app/Services/dc.service';
import { InventoryService } from 'src/app/Services/inventory.service';
import { UserService } from 'src/app/Services/user.service';

@Component({
  selector: 'app-update-nd-inv',
  templateUrl: './update-nd-inv.component.html',
  styleUrls: ['./update-nd-inv.component.css']
})
export class UpdateNdInvComponent implements OnInit {
  pmEs: pME= {
    article: '',
    magasinName: '',
    emplacementName: '',
    quantity: 0
  } ;
  ComptageDetails: CM = {
    article: '',
    production: 0,
    userName: '',
    weekNB: 0,
    pmEs: [] 
  }
  notifs: Notif[];
  notif: Notif = {
    article : '',
    weekNB : 0,
    ecartPrix: 0,
    ecartPiece: 0,
    ecartPerCent : 0,
    accepted: false,
    justif: ''
  }
  aw: ArticleWeek;
  users : User[] = [];
  update: boolean= false;
  stock: boolean= false;
  constructor(private route: ActivatedRoute,private dcService: DCService,private userService: UserService ,private inventoryService:InventoryService, private comptageServise: ComptageService, private router: Router) { }
  Article: string;
  WeekNB: number;
  Ec: ecart[];
  ecars: ecart[];
  weekNBasString: string;
  acc: boolean= false ;
  ngOnInit(): void {
    this.route.paramMap.subscribe({
      next: (params) => {
        this.Article = params.get('article');
        this.WeekNB = parseInt(params.get('weekNB'), 10);
        if(this.Article && !isNaN(this.WeekNB)) {
          this.comptageServise.getNdComptage(this.Article, this.WeekNB)
          .subscribe({
            next: (CM) => {
              this.ComptageDetails = CM;
              if (this.ComptageDetails.userName != ''){
                this.stock = true;
                console.log(this.stock);
              }
            },
            error: (Response) => {
              console.log(Response)
            }
          })
          this.dcService.getNotif(this.Article,this.WeekNB).subscribe({
            next: (Notif) => {
              this.notif = Notif;
              if(this.notif.accepted == true ){
                this.acc = true;
                console.log(this.acc);
              }
            },
            error: (error) => {
              console.log( error);
            }
              
          });
          this.inventoryService.getNdEcart().subscribe({
            next: (ecars) => {
              this.Ec = ecars.filter(item => item.article==this.Article && item.weekNB ==this.WeekNB); 
              console.log(this.Ec.length);
              if ((this.Ec[0].ecartPerCent<-5) || (this.Ec[0].ecartPrice<-500)) {
                this.update = false;
              }
              console.log(this.update);
            },
            error: (error) => {
              console.log('Error fetching inventories:', error);
            }
          });
        }
      }
    })
  }  
  UpdateLoc(){
    console.log(this.Article);
    this.router.navigate(['/ResponsableMagasin/UpdateLocation/', this.Article]);
  }
  SendNotif(){
    this.inventoryService.getEcart().subscribe({
      next: (ecars) => {
        this.Ec = ecars.filter(item => item.article==this.Article && item.weekNB ==this.WeekNB); 
        console.log(this.Ec.length);
      },
      error: (error) => {
        console.log('Error fetching inventories:', error);
      }
    });
    this.notif.article = this.Article;
    this.notif.weekNB = this.WeekNB;
    this.notif.ecartPiece = this.Ec[0].ecart;
    this.notif.ecartPrix = this.Ec[0].ecartPrice;
    this.notif.ecartPerCent = this.Ec[0].ecartPerCent;
    this.notif.accepted = false;
    this.dcService.sendNotif(this.notif).subscribe({
      next: (Notif) => {
        alert("Notification sent successfully");
      },
      error: (error) => {
        console.log('Error:', error);
      }
    })
  }
}
