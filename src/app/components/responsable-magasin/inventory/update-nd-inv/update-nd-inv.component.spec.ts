import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateNdInvComponent } from './update-nd-inv.component';

describe('UpdateNdInvComponent', () => {
  let component: UpdateNdInvComponent;
  let fixture: ComponentFixture<UpdateNdInvComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateNdInvComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UpdateNdInvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
