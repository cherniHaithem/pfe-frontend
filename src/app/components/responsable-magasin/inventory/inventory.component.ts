import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InvPerWeek } from 'src/app/Models/InvPerWeek';
import { Inventory } from 'src/app/Models/Inventory.interface';
import { InventoryService } from 'src/app/Services/inventory.service';
import { InvPW } from 'src/app/Models/InvPW'
import { ArticleWeek } from 'src/app/Models/ArticleWeek';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {
  showInput = false;
  showForm = false;
  Year: number;
  article: string;
  weekNB: number;
  inventories: Inventory[] = [];
  aw: ArticleWeek= {
    article: '',
    weekNB: 0,
  };
  constructor(private inventoryService: InventoryService,private router: Router) { }

  ngOnInit(): void {
    this.inventoryService.getInventories().subscribe ({
      next: (inventories) => {
        this.inventories = inventories; 
      },
      error: (Response) => {
        console.log('Error: ', Error);
      }
    });
  }
  addInventories() {
    this.showInput = true;
  }
  
  AddLine(){
    console.log(this.aw)
    this.inventoryService.postInvLine(this.aw).subscribe({
      next: (response) => {
      console.log(this.aw);
      alert('invLine created');
      console.log('Response:', response); 
    },
    error: (error) => {
      console.log('Error:', error);
      alert('Error creating invLine');
      }
    })
    this.inventoryService.deleteInv(this.aw).subscribe({
      next: (InvPerWeek) => {
        alert('next one deleted');
      },
      error: (Response) => {
        console.log('Error: ', Error);
      }
    })
  }
  /*selectProducts(){
    if (!this.Year) {
      alert('Please enter a valid year');
      console.error('Please enter a valid year');
      return;
    }
    this.router.navigate(['../ResponsableMagasin/NewInv']);
  }*/
  AddInv(){
    if (!this.Year) {
      alert('Please enter a valid year');
      console.error('Please enter a valid year');
      return;
    }
    else{
      this.inventoryService.AddInventories(this.Year).subscribe({
        next: (any) =>{
          alert('Inventory add');
        },
        error: (Error) => {
          console.log('Error: ', Error);
        }
      })
    }
  }
  addInventoryLine(){
    this.showForm = true;
  }
  
}
