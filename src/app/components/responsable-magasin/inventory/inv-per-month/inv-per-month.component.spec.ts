import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvPerMonthComponent } from './inv-per-month.component';

describe('InvPerMonthComponent', () => {
  let component: InvPerMonthComponent;
  let fixture: ComponentFixture<InvPerMonthComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvPerMonthComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InvPerMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
