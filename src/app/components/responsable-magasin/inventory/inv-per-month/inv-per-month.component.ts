import { Component, OnInit } from '@angular/core';
import { Inventory } from 'src/app/Models/Inventory.interface';
import { InventoryService } from 'src/app/Services/inventory.service';

@Component({
  selector: 'app-inv-per-month',
  templateUrl: './inv-per-month.component.html',
  styleUrls: ['./inv-per-month.component.css']
})
export class InvPerMonthComponent implements OnInit {
  inventories: Inventory[] = [];
  selectedMonth: string = "janvier";
  
  index: number = 0;
  Months: string[] = ['janvier', 'fevrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  constructor(private inventoryService: InventoryService) { }

  ngOnInit(): void {
    this.inventoryService.getInvByMonth(this.selectedMonth).subscribe ({
      next: (inventories) => {
        this.inventories = inventories; 
        console.log(inventories)
      },
      error: (Response) => {
        console.log('Error: ', Error);
      }
    });
  }
  onMonthDecreese(selectedMonth: string) {
    this.index +=1,
    this.selectedMonth = this.Months[this.index],
    console.log(this.selectedMonth)
    this.inventoryService.getInvByMonth(this.selectedMonth).subscribe ({
      next: (inventories) => {
        this.inventories = inventories; 
        console.log(inventories)

      },
      error: (Response) => {
        console.log('Error: ', Error);
      }
    });
      
  }
  onMonthIncreese(selectedMonth: string) {
    this.index -=1,
    this.selectedMonth = this.Months[this.index],
    console.log(this.selectedMonth)
    this.inventoryService.getInvByMonth(this.selectedMonth).subscribe ({
      next: (inventories) => {
        this.inventories = inventories; 
        console.log(inventories)

      },
      error: (Response) => {
        console.log('Error: ', Error);
      }
    });
  }
 

}
