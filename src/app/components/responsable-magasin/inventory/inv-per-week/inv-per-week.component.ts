import { Component, OnInit } from '@angular/core';
import { InvPerWeek } from 'src/app/Models/InvPerWeek';
import { InventoryService } from 'src/app/Services/inventory.service';

@Component({
  selector: 'app-inv-per-week',
  templateUrl: './inv-per-week.component.html',
  styleUrls: ['./inv-per-week.component.css']
})
export class InvPerWeekComponent implements OnInit {
  Invs: InvPerWeek[] = [];
  NBWeek: number = 1;
  index: number = 0;
  totalWeeks: number = 48;
  currentPage: number = 1;
  constructor(private inventoryService: InventoryService) { }

  ngOnInit(): void {
    

    this.inventoryService.getInvPerWeek(this.currentPage).subscribe ({
      next: (Invs) => {
        this.Invs = Invs; 
      },
      error: (Response) => {
        console.log('Error: ', Error);
      }
    });
  }
  loadInventories(): void{
    this.inventoryService.getInvPerWeek(this.currentPage).subscribe ({
      next: (Invs) => {
        this.Invs = Invs; 
      },
      error: (Response) => {
        console.log('Error: ', Error);
      }
    });
  }
  onPageChanged(pageNumber: number): void {
    this.currentPage = pageNumber;
    this.loadInventories();
  }

}
