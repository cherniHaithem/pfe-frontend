import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvPerWeekComponent } from './inv-per-week.component';

describe('InvPerWeekComponent', () => {
  let component: InvPerWeekComponent;
  let fixture: ComponentFixture<InvPerWeekComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvPerWeekComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InvPerWeekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
