import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvPerYearComponent } from './inv-per-year.component';

describe('InvPerYearComponent', () => {
  let component: InvPerYearComponent;
  let fixture: ComponentFixture<InvPerYearComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvPerYearComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InvPerYearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
