import { Component, OnInit } from '@angular/core';
import { Inventory } from 'src/app/Models/Inventory.interface';
import { InventoryService } from 'src/app/Services/inventory.service';

@Component({
  selector: 'app-inv-per-year',
  templateUrl: './inv-per-year.component.html',
  styleUrls: ['./inv-per-year.component.css']
})
export class InvPerYearComponent implements OnInit {
  inventories: Inventory[] = [];
  constructor(private inventoryService: InventoryService) { }

  ngOnInit(): void {
    this.inventoryService.getInventories().subscribe ({
      next: (inventories) => {
        this.inventories = inventories; 
      },
      error: (Response) => {
        console.log('Error: ', Error);
      }
    });
  }

}
