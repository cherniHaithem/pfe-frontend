import { Component, OnInit } from '@angular/core';
import { DCGet } from 'src/app/Models/DCGet';
import { DCService } from 'src/app/Services/dc.service';

@Component({
  selector: 'app-nd-inv',
  templateUrl: './nd-inv.component.html',
  styleUrls: ['./nd-inv.component.css']
})
export class NdInvComponent implements OnInit {
  dcGets: DCGet[] = [];
  constructor(private dcService: DCService) { }

  ngOnInit(): void {
    this.dcService.getNdComptage().subscribe ({
      next: (dcGets) =>{
        this.dcGets = dcGets;
        console.log(this.dcGets);
      },
      error: (Response) => {
        console.log('Error: ', Error);
      }

    })
  }

}
