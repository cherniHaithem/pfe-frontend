import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NdInvComponent } from './nd-inv.component';

describe('NdInvComponent', () => {
  let component: NdInvComponent;
  let fixture: ComponentFixture<NdInvComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NdInvComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NdInvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
