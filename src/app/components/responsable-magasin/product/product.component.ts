import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/Models/product.interface';
import { ProductService } from '../../../Services/product.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Services/auth.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  products: Product[] = [];
  constructor(private productService: ProductService, private router: Router, private authService: AuthService) { }
  
  ngOnInit(): void {
    /*this.authService.getUserRole().subscribe({
      next: (data) =>{
        this.userRole = data.role;
      },
      error: (Response) => {
        console.log('Error fetching user role:', Response);
      }
    })*/
    /*this.authService.userRole$.subscribe(role => {
      this.userRole = role; // Update the userRole property when it changes
    });*/
    this.productService.getAllProducts()
    .subscribe({
      next: (products) => {
        this.products = products;
      },
      error: (Response) => {
        console.log(Response)
      }
    })
  }
  goToAdd() {
    this.router.navigate(['/ResponsableMagasin/AddProduct']);

  }
  
  
}
