import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticleWeek } from 'src/app/Models/ArticleWeek';
import { CM } from 'src/app/Models/CM';
import { DC } from 'src/app/Models/DC';
import { ecart } from 'src/app/Models/ecart';
import { pME } from 'src/app/Models/pME';
import { User } from 'src/app/Models/user.interface';
import { ComptageService } from 'src/app/Services/comptage.service';
import { DCService } from 'src/app/Services/dc.service';
import { InventoryService } from 'src/app/Services/inventory.service';
import { UserService } from 'src/app/Services/user.service';

@Component({
  selector: 'app-comptage',
  templateUrl: './comptage.component.html',
  styleUrls: ['./comptage.component.css']
})
export class ComptageComponent implements OnInit {
  pmEs: pME= {
    article: '',
    magasinName: '',
    emplacementName: '',
    quantity: 0
  } ;
  ComptageDetails: CM = {
    article: '',
    production: 0,
    userName: '',
    weekNB: 0,
    pmEs: [] 
  }
  aw: ArticleWeek;
  users : User[] = [];
  update: boolean= false;
  stock: boolean= false;
  constructor(private route: ActivatedRoute,private dcService: DCService,private userService: UserService ,private inventoryService:InventoryService, private comptageServise: ComptageService, private router: Router) { }
  Article: string;
  WeekNB: number;
  Ec: ecart[];
  ecars: ecart[];
  weekNBasString: string;
  dc: DC = {
    article: '',
    userName: '',
    weekNB: 0,
  };
  ngOnInit(): void {
    this.route.paramMap.subscribe({
      next: (params) => {
        this.Article = params.get('article');
        this.WeekNB = parseInt(params.get('weekNB'), 10);
        if(this.Article && !isNaN(this.WeekNB)) {
          this.comptageServise.getComptage(this.Article, this.WeekNB)
          .subscribe({
            next: (CM) => {
              this.ComptageDetails = CM;
              if (this.ComptageDetails.userName != ''){
                this.stock = true;
              }
            },
            error: (Response) => {
              console.log(Response)
            }
          })
          this.inventoryService.getEcart().subscribe({
            next: (ecars) => {
              this.Ec = ecars.filter(item => item.article==this.Article && item.weekNB ==this.WeekNB); 
              console.log(this.Ec.length);
              console.log(this.Ec)
              if ((this.Ec[0].ecartPerCent<-5) || (this.Ec[0].ecartPrice<-500)) {
                this.update = true;
                
              }
              console.log(this.update);
            },
            error: (error) => {
              console.log('Error fetching inventories:', error);
            }
          });
        }
      }
    })
    this.userService.getAllUsers().subscribe ({
      next: (users) => {
        this.users = users.filter(item => item.role == "AgentInventaire");
        console.log(this.users);
      },
      error: (Response) => {
        console.log(Response)
      }
    })
  }
  selectUser(user: { userName: string }) {
    this.dc.userName = user.userName
    console.log(this.dc.userName)
  }
  DCInv() {
    this.dcService.addDC(this.dc).subscribe ({
      next: (any) =>{
        alert('2nd counting created successfully');
      },
      error: (error) => {
        console.error('An error occurred:', error);
        
      }
    });
  }
  UpdateLoc(){
    this.router.navigate(['../ResponsableMagasin/UpdateLocation/', this.Article]);
    console.log(this.dc.article);
  }

}
