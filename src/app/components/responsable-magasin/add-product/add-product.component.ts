import { Component, OnInit } from '@angular/core';
import { ReferenceService } from 'src/app/Services/reference.service';
import { Router } from '@angular/router';
import { reference} from 'src/app/Models/reference.interface';
import { Product } from 'src/app/Models/product.interface';
import { ProductService } from 'src/app/Services/product.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  references: reference[] = [];
  addProduct: Product = {
    article: '',
    description: '',
    referenceName: '',
    price: 0
  }

  constructor(private refrenceService: ReferenceService, private productService: ProductService, private router: Router) { }

  ngOnInit(): void {
    this.refrenceService.getAllReferences()
    .subscribe({ 
      next: (references) => {
        this.references = references;
      },
      error: (Response) => {
        console.log(Response)
      }
    })
  }
  addproduct(){
    console.log(this.addProduct)
    this.productService.postProduct(this.addProduct).subscribe({
      next: (Product) => {
        console.log(Product)
        this.router.navigate(['../ResponsableMagasin/Product']);
      },
      error: (error) => {
        console.error('An error occurred while deleting the product:', error);
        alert("Failed to add product");
        this.router.navigate(['../ResponsableMagasin/Product']);
      }
    })
    
  }

}
