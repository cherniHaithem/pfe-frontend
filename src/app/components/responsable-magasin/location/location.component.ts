import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from 'src/app/Services/product.service';
import { pME } from 'src/app/Models/pME'; 
import { LocationService } from 'src/app/Services/location.service';
@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {
  pmes: pME[] =[] ;
  pme: pME= {
    article: '',
    magasinName: '',
    emplacementName: '',
    quantity: 0
  };
  constructor(private route: ActivatedRoute,private locationService: LocationService, private productService: ProductService, private router: Router) { }
  article: string;
  loc = false;
  ngOnInit(): void {
    this.route.paramMap.subscribe({
      next: (params) => {
        this.article = params.get('article');
        if(this.article) {
          this.productService.getLocation(this.article)
          .subscribe({
            next: (pmes) => {
              this.pmes = pmes;
              console.log(this.article);
              console.log(this.pmes)
            },
            error: (Response) => {
              console.log('Error: ', Response);
              alert(Response);
            } 
          })
        }
      }
    })
  }
  showLocation(){
    this.loc = true;
  }
  AddLocation(){
    this.pme.article = this.article;
    console.log(this.pme);
    this.locationService.addLocation( this.pme).subscribe({
      next: (any) => {
        alert('Location saved successfully')
      }
    })
  }
}
