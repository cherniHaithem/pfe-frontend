import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponsableMagasinComponent } from './responsable-magasin.component';

describe('ResponsableMagasinComponent', () => {
  let component: ResponsableMagasinComponent;
  let fixture: ComponentFixture<ResponsableMagasinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResponsableMagasinComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResponsableMagasinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
