import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { pME } from 'src/app/Models/pME';
import { LocationService } from 'src/app/Services/location.service';
import { ProductService } from 'src/app/Services/product.service';

@Component({
  selector: 'app-update-location',
  templateUrl: './update-location.component.html',
  styleUrls: ['./update-location.component.css']
})
export class UpdateLocationComponent implements OnInit {
  Article: string;
  pmEs: pME[]=[];
  newpME: pME[]=[];
  constructor(private route: ActivatedRoute, private router: Router,private locationService: LocationService, private productService:ProductService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe({
      next: (params) => {
        this.Article = params.get('article');
        console.log('Article:',this.Article);
        this.productService.getLocation(this.Article).subscribe({
          next: (pmEs) => {
            this.pmEs = pmEs;
            console.log(this.pmEs)
          },
          error: (error) => {
            console.error( error);
            
          }
        })
      }
    })  
  }
  Save(){
    this.locationService.updateLocation(this.pmEs).subscribe({
      next: (pmEs) =>{
        alert('Quantities updated successfully!');
      }
    })
  }

}
